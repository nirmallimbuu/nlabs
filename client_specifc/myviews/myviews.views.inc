<?php

/**
 * Form : User Settings form: Main
 */

/**
 * Implement hook_views_data().
 *
 * Describe table "houses" so that it can be used by views
 */
function myviews_views_data() {
  $data = array();

  // Group the fields
  $data['houses']['table'] = array(
      'group' => t("Houses"),
  );
  // Show our table in views
  $data['houses']['table']['base'] = array(
      'field' => 'hoid',
      'title' => 'Houses',
      'weight' => 12,
  );

  // join with users table
  $data['houses']['table']['join']['users'] = array(
      'left_table' => 'users',
      'left_field' => 'uid',
      'field' => 'uid',
  );
  $data['houses']['owner'] = array(
      'title' => 'House owner',
      'help' => t('The owner of this property/house.'),
      'relationship' => array(
          'title' => 'owner',
          'handler' => 'views_handler_relationship',
          'base' => 'users',
          'field' => 'uid',
          'label' => t('House Owner'),
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );


  // Add fields
  // add House ID (hoid)
  $data['houses']['hoid'] = array(
      'title' => t('House ID'),
      'help' => t('House record ID.'),
      'filter' => array(
          'handler' => 'views_handler_filter',
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );
  // Add House Title (tile)
  $data['houses']['title'] = array(
      'title' => t('Houser/property title'),
      'help' => t('The title of this house/property.'),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );


  // Global Header Area : Collapsible area
  // custom area handler
  $data['views']['collapsible_area'] = array(
      'title' => t('Collabsible Text area'),
      'help' => t('Provide collabsible markup text for the area.'),
      'area' => array(
          'handler' => 'myviews_handler_collapsible_area_text',
      ),
  );


  return $data;
}

/**
 * 
 * Implements hook_views_default_views()
 * 
 * @return view
 */
function myviews_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'houses';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'houses';
  $view->human_name = 'houses';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'houses';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Houses: owner */
  $handler->display->display_options['relationships']['owner']['id'] = 'owner';
  $handler->display->display_options['relationships']['owner']['table'] = 'houses';
  $handler->display->display_options['relationships']['owner']['field'] = 'owner';
  $handler->display->display_options['relationships']['owner']['required'] = TRUE;
  /* Field: Houses: House ID */
  $handler->display->display_options['fields']['hoid']['id'] = 'hoid';
  $handler->display->display_options['fields']['hoid']['table'] = 'houses';
  $handler->display->display_options['fields']['hoid']['field'] = 'hoid';
  $handler->display->display_options['fields']['hoid']['label'] = '';
  $handler->display->display_options['fields']['hoid']['element_label_colon'] = FALSE;
  /* Field: Houses: Houser/property title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'houses';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'owner';
  $handler->display->display_options['fields']['name']['label'] = 'Owner';
  /* Sort criterion: Houses: Houser/property title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'houses';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  $handler->display->display_options['sorts']['title']['expose']['label'] = 'Houser/property title';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['path'] = 'houses';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Houses';
  $handler->display->display_options['menu']['description'] = 'Custom Table Views';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
      'hoid' => 'hoid',
      'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
      'hoid' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
      'title' => array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Houses: House ID */
  $handler->display->display_options['fields']['hoid']['id'] = 'hoid';
  $handler->display->display_options['fields']['hoid']['table'] = 'houses';
  $handler->display->display_options['fields']['hoid']['field'] = 'hoid';
  $handler->display->display_options['fields']['hoid']['label'] = 'ID';
  $handler->display->display_options['fields']['hoid']['element_label_colon'] = FALSE;
  /* Field: Houses: Houser/property title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'houses';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['path'] = 'houses/table';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Houses (table)';
  $handler->display->display_options['menu']['description'] = 'Custom Table Views';
  $handler->display->display_options['menu']['weight'] = '3';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['houses'] = array(
      t('Master'),
      t('houses'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('House Owner'),
      t('Owner'),
      t('Houser/property title'),
      t('Page'),
      t('ID'),
      t('Title'),
  );


  $views[$view->name] = $view;

  return $views;
}