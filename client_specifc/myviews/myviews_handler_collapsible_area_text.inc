<?php



/**
 * 
 * Global : Collapsible Area handler
 * 
 * This is used in Header section
 * 
 */
class myviews_handler_collapsible_area_text extends views_handler_area_text {
  function render($empty = FALSE) {
    // Here you just return a string of your content you want.
    if ($render = parent::render($empty)) {
      $element = array(
        '#type' => 'fieldset',
        '#title' => t('Title'),
        '#value' => $render,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#children' => '',
      );
      $output = theme('fieldset', array('element' => $element));
      return $output;
    }
  }
}