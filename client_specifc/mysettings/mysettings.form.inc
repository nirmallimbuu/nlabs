<?php

/**
 * Form : User Settings form: Main
 */
function mysettings_admin_page($form, &$form_state, $account) {
  global $language;
  
  // reference to account we are editing
  // on form first load
  if (!$form_state['rebuild']) {
    // first load
    $form_state['edit_account'] = array(
        '#type' => 'value',
        '#value' => $account,
    );
  } else {
    // later
    $account = user_load($account->uid);
    $form_state['edit_account'] = array(
        '#type' => 'value',
        '#value' => $account,
    );
  }
  
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  
  // Email field ----------------------------
  $form['email-wrapper'] = array(
      '#prefix' => '<div id="email-wrapper-id"><label>'. t('Email') . ': </label>',
      '#suffix' => '</div>',
  );
  $form['email-wrapper']['email'] = array(
      '#title' => t('Email'),
      '#type' => 'markup',
      '#markup' => $account->mail,
  );
  // Email edit button ----------------------
  $form['email-wrapper']['email-edit'] = array(
      '#type' => 'button',
      '#value' => 'edit',
      '#btname' => 'email-edit',
      '#name' => 'email-edit',
      '#ajax' => array(
          'callback' => 'mysettings_form_email_edit_callback',
          'wrapper' => 'email-wrapper-id',
          'method' => 'replace',
          'effect' => 'none',
          'event' => 'click',
      ),
  );
  
  // Language field ----------------------------
  $all_langs_list = language_list();
  // language to choose from
  $all_langs = array();
  foreach ($all_langs_list as $alang) {
    $all_langs[$alang->language] = t($alang->name);
  }
  $user_lang = t($language->name);
  // language field
  $form['lang-wrapper'] = array(
      '#prefix' => '<div id="lang-wrapper-id"><label>'. t('Language') . ': </label>',
      '#suffix' => '</div>',
  );
  $form['lang-wrapper']['language'] = array(
      '#type' => 'markup',
      '#markup' => $user_lang,
  );
  // Language edit button ----------------------
  $form['lang-wrapper']['lang-edit'] = array(
      '#type' => 'button',
      '#value' => 'edit',
      '#btname' => 'lang-edit',
      '#name' => 'lang-edit',
      '#ajax' => array(
          'callback' => 'mysettings_form_email_edit_callback',
          'wrapper' => 'lang-wrapper-id',
          'method' => 'replace',
          'effect' => 'none',
          'event' => 'click',
      ),
  );
  
  
  
  // Picture field ----------------------------
  $default_image = 'public://pictures/profile_default.png';
  if ($account->picture) {
    $picture_info = $account->picture;
    $url = file_create_url($picture_info->uri);
    $picture = theme('image_style', array('style_name' => 'thumbnail', 'path' => $picture_info->uri));
  } else {
    $url = file_create_url($default_image);
    $picture = theme('image_style', array('style_name' => 'thumbnail', 'path' => USER_IMG_PATH));
  }
  $form['picture-wrapper'] = array(
      '#prefix' => '<div id="picture-wrapper-id"><label>'. t('Picture') . ': </label>',
      '#suffix' => '</div>',
  );
  $form['picture-wrapper']['picture'] = array(
      '#type' => 'markup',
      '#markup' => $picture,
  );
  // Picture edit button ----------------------
  $form['picture-wrapper']['picture-edit'] = array(
      '#type' => 'button',
      '#value' => 'edit',
      '#btname' => 'picture-edit',
      '#name' => 'picture-edit',
      '#ajax' => array(
          'callback' => 'mysettings_form_email_edit_callback',
          'wrapper' => 'picture-wrapper-id',
          'method' => 'replace',
          'effect' => 'none',
          'event' => 'click',
      ),
  );
  
  // Password field ----------------------------
  $form['pass-wrapper'] = array(
      '#prefix' => '<div id="pass-wrapper-class"><label>' . t('Password') . ' :  ********</label>',
      '#suffix' => '</div>',
  );
  // Password edit button ----------------------
  $form['pass-wrapper']['pass-edit'] = array(
      '#type' => 'button',
      '#value' => 'edit',
      '#btname' => 'pass-edit',
      '#name' => 'pass-edit',
      '#ajax' => array(
          'callback' => 'mysettings_form_email_edit_callback',
          'wrapper' => 'pass-wrapper-class',
          'method' => 'replace',
          'effect' => 'none',
          'event' => 'click',
      ),
  );
  
  // ajax save fields ------------------------ AFTER ----------
  if (!empty($form_state['values'])) {
    
    // Get email save field, only upon email button clicks
    if ($trigger_element == 'email-edit' || $trigger_element == 'email-close' || $trigger_element == 'email-save') {
      // Email save field ------------------------
      $form['email-saver'] = array(
          '#prefix' => '<div id="email-saver-id">',
          '#suffix' => '</div>',
      );
      $form['email-saver']['email-text'] = array(
          '#title' => t('Email'),
          '#type' => 'textfield',
          '#value' => $account->mail,
          '#element_validate' => array('mysettings_form_email_email_validate'),
      );
      // Save button
      $form['email-saver']['email-save'] = array(
          '#type' => 'button',
          '#value' => 'save',
          '#btname' => 'email-save',
          '#name' => 'email-save',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_save_callback',
              'wrapper' => 'email-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );
      // Close button
      $form['email-saver']['email-close'] = array(
          '#type' => 'button',
          '#value' => 'close',
          '#btname' => 'email-close',
          '#name' => 'email-close',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_close_callback',
              'wrapper' => 'email-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );//end-email-save-field --
    }//end-email-edit-save
    
    
    // Get language list save field
    if ($trigger_element == 'lang-edit' || $trigger_element == 'lang-close' || $trigger_element == 'lang-save') {
      // Email save field ------------------------
      $form['lang-saver'] = array(
          '#prefix' => '<div id="lang-saver-id">',
          '#suffix' => '</div>',
      );
      $form['lang-saver']['lang-text'] = array(
          '#type' => 'select',
          '#title' => t('Choose a language'),
          '#options' => $all_langs,
          '#default_value' => $language->language,
      );
      // Save button
      $form['lang-saver']['lang-save'] = array(
          '#type' => 'button',
          '#value' => 'save',
          '#btname' => 'lang-save',
          '#name' => 'lang-save',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_save_callback',
              'wrapper' => 'lang-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );
      // Close button
      $form['lang-saver']['lang-close'] = array(
          '#type' => 'button',
          '#value' => 'close',
          '#btname' => 'lang-close',
          '#name' => 'lang-close',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_close_callback',
              'wrapper' => 'lang-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );//end-lang-save-field --
    }//end-lang-edit-save
    
    
    // Get Picture upload field, only upon button related to picture is clicked
    if ($trigger_element == 'picture-edit' || $trigger_element == 'picture-close' || $trigger_element == 'picture-save') {
      $form['picture-saver']['picture_field'] = array(
          '#type' => 'managed_file',
          '#description' => t('Allowed file types: png,gif,jpg jpeg <br /> Files must be less than 2 MB. '),
          '#upload_location' => 'public://pictures/',
      );
      $form['picture-saver']['picture_field']['#upload_validators']['file_validate_extensions'][0] = 'png jpg gif jpeg';
      $form['picture-saver']['picture_field']['#upload_validators']['file_validate_size'][0] = '2097152';
    }
    
    // Get password save field, only upon password button clicks
    if ($trigger_element == 'pass-edit' || $trigger_element == 'pass-close' || $trigger_element == 'pass-save') {
      // Password save field ------------------------
      $form['pass-saver'] = array(
          '#prefix' => '<div id="pass-saver-id">',
          '#suffix' => '</div>',
      );
      $form['pass-saver']['password10'] = array(
          '#title' => t('Current Password'),
          '#type' => 'password',
          '#description' => t('Enter your current password: '),
          '#element_validate' => array('mysettings_form_email_pass_validate'),
      );
      $form['pass-saver']['password11'] = array(
          '#type' => 'password_confirm',
      );
      // Save button
      $form['pass-saver']['pass-save'] = array(
          '#type' => 'button',
          '#value' => 'save',
          '#btname' => 'pass-save',
          '#name' => 'pass-save',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_save_callback',
              'wrapper' => 'pass-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );
      // Close button
      $form['pass-saver']['pass-close'] = array(
          '#type' => 'button',
          '#value' => 'close',
          '#btname' => 'pass-close',
          '#name' => 'pass-close',
          '#ajax' => array(
              'callback' => 'mysettings_form_email_close_callback',
              'wrapper' => 'pass-saver-id',
              'method' => 'replace',
              'effect' => 'none',
              'event' => 'click',
          ),
      );//end-pass-save-field --
    }//end-pass-edit-check
    
    
  }//end-ajax-save-field -----------------------------
  
  
  

  return $form;
}

/**
 * Form : User Settings form: Validate callback
 */
function mysettings_admin_page_validate($form, &$form_state) {
  $inputs = $form_state['input'];
  $values = $form_state['values'];
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  // Validate triggering elements
  switch ($trigger_element) {
    case 'email-save':
      break;
    case 'lang-save':
      break;
    case 'pass-save':
      break;
  }
  
}

/**
 * Form: User Settings form: Submit callback
 * @param type $form
 * @param type $form_state
 */
function mysettings_admin_page_submit($form, &$form_state) {
  // get all values
  $values = $form_state['values'];
  
  drupal_set_message(t("Changes successfully changed"));
}


/**
 * Form : Edit callback : mysettings_admin_page
 */
function mysettings_form_email_edit_callback($form, &$form_state) {
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  
  // Act accordingly
  switch ($trigger_element) {
    case 'email-edit':
      $form_state['form_state']['#value'] = 'edit-email';
      return $form['email-saver'];
      break;
    case 'lang-edit':
      return $form['lang-saver'];
      break;
    case 'picture-edit':
      return $form['picture-saver'];
      break;
    case 'pass-edit':
      return $form['pass-saver'];
      break;
  }
}
/**
 * Form : Edit callback : mysettings_admin_page
 */
function mysettings_form_email_save_callback($form, &$form_state) {
  global $base_url;
  
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  // Get the account (saved earlier)
  $account = $form_state['edit_account']['#value'];
  
  // Act accordingly
  switch ($trigger_element) {
    case 'email-save':
      // only save if some value is entered
      if (!empty($form_state['input']['email-text'])) {
        $account->mail = $form_state['input']['email-text'];
        if (!user_save($account)) {
          // nothing saved, return same element
          return $form['email-wrapper']; 
        }
        $form['email-wrapper']['email']['#markup'] = $account->mail;
        return $form['email-wrapper'];
      }
      return $form['email-saver'];
      break;
    case 'lang-save':
      // only save if some value is entered
      if (!empty($form_state['input']['lang-text'])) {
        $account->language = $form_state['input']['lang-text'];
        if (!user_save($account)) {
          // nothing saved, return same element
          return $form['lang-wrapper']; 
        }
        // get the string (not "en")
        $all_langs = language_list();
        foreach ($all_langs as $alang) {
          if ($alang->language ==  $form_state['input']['lang-text']) {
            $user_lang = $alang->name;
          }
        }
        $form['lang-wrapper']['language']['#markup'] = $user_lang;
        return $form['lang-wrapper'];
      }
      return $form['lang-saver'];
      break;
    case 'pass-save':
      // only save if some value is entered (confirm_password has internal two elements : pass1 and pass2
      if (($form_state['input']['password11']['pass1']) != '' && ($form_state['input']['password11']['pass2']) != '' &&
              ($form_state['input']['password11']['pass1'] == $form_state['input']['password11']['pass2']) &&
              ($form_state['input']['password10'] != '')) {
//            $account->pass = $form_state['input']['password11'];
            // save user
            //user_save($account);
            // nothing saved, return same element
            print "<pre>";
            print_r("WTF");
            print "</pre>";
            die;
        return $form['pass-wrapper'];
      }
      return $form['pass-saver'];
//      $account->pass = $form_state['input']['password11'];
//      return $form_state['pass-wrapper'];
      break;
  }
  
}
/**
 * Form : Close callback : mysettings_admin_page
 */
function mysettings_form_email_close_callback($form, &$form_state) {
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  // Act accordingly
  switch ($trigger_element) {
    case 'email-close':
      return $form['email-wrapper'];
      break;
    case 'lang-close':
      return $form['lang-wrapper'];
      break;
    case 'pass-close':
      return $form['pass-wrapper'];
      break;
  }
}


/**
 * Validate Email field only
 */
function mysettings_form_email_email_validate($form, &$form_state) {
  $values = $form_state['values'];
  $inputs = $form_state['input'];
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  // Only for pass field
  if ($trigger_element == 'email-save') {
    if (empty($inputs['email-text'])) {
      form_set_error('email-text', t('Empty email field'));
    }
  }
}

/**
 * Validate Language field only
 */
function mysettings_form_email_lang_validate($form, &$form_state) {
  $values = $form_state['values'];
  $inputs = $form_state['input'];
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
}

/**
 * Validate Password field only
 */
function mysettings_form_email_pass_validate($form, &$form_state) {
  $values = $form_state['values'];
  $inputs = $form_state['input'];
  // Get triggering element
  $trigger_element = mysettings_form_get_trigger($form_state);
  // Only for pass field
  if ($trigger_element == 'pass-save') {
    // previous password
    if (empty($inputs['password10'])) {
      form_set_error('password10', t('Password field is required : current pass'));
    }
    // confirm passwords
    if (empty($inputs['password11']['pass1'])) {
      form_set_error('password11', t('Password 2 (pass1) field is required'));
    }
    if (empty($inputs['password11']['pass2'])) {
      form_set_error('password11', t('Confirm password field is empty'));
    }
    // confirm passwords same
    if ($inputs['password11']['pass1'] != $inputs['password11']['pass2']) {
      form_set_error('password11', t('News passwords did not match'));
    }
  }
}

/**
 * Get triggering element form $form_state
 * @param type $form_state
 */
function mysettings_form_get_trigger($form_state) {
  $trigger_element = '';
  // check : empty in first form build
  if (!empty($form_state['values'])) {
    $trigger_element = $form_state['triggering_element']['#btname'];
    if (isset($form_state['input']['_triggering_element_name'])) {
      $trigger_element = $form_state['input']['_triggering_element_name'];
    }
  }
  return $trigger_element;
}