<?php


/**
 * 
 * This file contains all the settings values for this module.
 * 
 */


/**
 * Get the list of formats
 * @return format_list(array)
 */
function mymails_get_formats() {
  
  $format_list = array(
      0 => 'simple',
      1 => 'html',
      2 => 'multipart',
  );
  
  return $format_list;
}

