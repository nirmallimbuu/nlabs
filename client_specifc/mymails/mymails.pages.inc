<?php

/**
 * Form: Admin page: Main
 */
function mymails_admin_page($form, $form_state) {
  global $user;
  $form = '';

  // get list of formats
  $format_list = mymails_get_formats();
  // settings
  $format = 'plain_text';


  // select a format to send
  $form['methods'] = array(
      '#type' => 'radios',
      '#title' => t('Choose the format of the mail.'),
      '#options' => $format_list,
      '#default_value' => 0,
      '#weight' => 1,
  );


  // message body
  $form['message'] = array(
      '#type' => 'text_format',
      '#title' => t('Message'),
      '#rows' => 6,
      '#weight' => 5,
      '#default_value' => '',
      '#resizable' => TRUE,
      '#format' => isset($format) ? $format : NULL,
  );


  // send button
  $form['send'] = array(
      '#type' => 'submit',
      '#value' => 'send',
      '#weight' => 10,
  );

  return $form;
}

/**
 * Form: Admin page: Validate callback
 * @param type $form
 * @param type $form_state
 */
function mymails_admin_page_validate($form, $form_state) {
  $inputs = $form_state['input'];
  if (empty($inputs['message']['value'])) {
    form_set_error('message', t('The message body cannot be empty'));
  }
}

/**
 * Form: Admin page: Submit callback
 * @param type $form
 * @param type $form_state
 */
function mymails_admin_page_submit($form, $form_state) {
  // get all inputs
  $values = $form_state['values'];

  // selected format
  $send_format_index = $values['methods'];
  // compare formats
  $send_callback = mymails_api_get_mail_callback($send_format_index);

  // send email
  call_user_func($send_callback);
}