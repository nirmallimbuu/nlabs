<?php

/**
 * 
 *  This file contains code to send emails.
 * 
 */

/**
 * Get the callback to use to send email
 * @param type $send_format_index
 * @return callback_function(string)
 */
function mymails_api_get_mail_callback($send_format_index) {
  switch ($send_format_index) {
    case '0':
      $callback = 'mymails_api_send_plain';
      break;
    case '1':
      $callback = 'mymails_api_send_html';
      break;
    case '2':
      $callback = 'mymails_api_send_multipart';
      break;
    default:
      $callback = 'mymails_api_send_plain';
      break;
  }
  
  return $callback;
}


/**
 * Send plain mail
 */
function mymails_api_send_plain() {
  
  $module = 'mymails';

//  drupal_mail($module, $key, $to, $language);
  drupal_set_message('Plain message send');
}


/**
 * Send plain mail
 */
function mymails_api_send_html() {
//  drupal_mail($module, $key, $to, $language);
  drupal_set_message('HTML message send');
}



/**
 * Send plain mail
 */
function mymails_api_send_multipart() {
//  drupal_mail($module, $key, $to, $language);
  drupal_set_message('Multpart message send');
}