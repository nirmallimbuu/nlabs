<?php
// $Id$
/**
 * A constant for the entity name.
 */
define('NLAB_ENTITY', 'nlab');

/**
 * A constant for the entity base table.
 */
define('NLAB_BASE_TBL', 'nlab');
/**
 * Primary key
 */
define('NLAB_BASE_TBL_PK', 'nlid');
/**
 * A constant for the entity base revision table.
 */
define('NLAB_BASE_REVISION_TBL', 'nlab_revision');




/**
 * A constant for the entity base table.
 */
define('NLAB_MODULENAME', 'nlab');




/**
 * Permission for nlab entity
 */
define('PERM_NLAB_ADMIN', 'administer '.NLAB_ENTITY.' entity');
define('PERM_NLAB_CREATE', 'create '.NLAB_ENTITY.' entity');
define('PERM_NLAB_EDIT', 'edit '.NLAB_ENTITY.' entity');
define('PERM_NLAB_DELETE', 'delete '.NLAB_ENTITY.' entity');

?>
