<?php

// Views integration

// 2.
/**
 * Implements hook_views_data()
 * 
 */
function nlab_views_data() {
  $data[NLAB_BASE_TBL]['table'] = array(
      'group' => t("NLab Feed"),
      'title' => t("NLab Feed Stream"),
      'help' => t("Shows social stream items targeted at the current user."),
  );
  // Show our table in views
  $data[NLAB_BASE_TBL]['table']['base'] = array(
      'field' => 'nlid',
      'title' => 'NLabfeed Items',
      'help' => t("NLab Feed items are generated through user activity."),
      'weight' => 10,
  );
  
  // Add new field 'body' to the add field list
  //
  $data[NLAB_BASE_TBL]['body'] = array(
    'title' => t('Body User'),
    'help' => t('User that created the action.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_current',
      'type' => 'yes-no',
    ),
  );
  
  // Add Bundle filter
  //
  $data[NLAB_BASE_TBL]['type'] = array(
      'title' => t('Bundle'),
      'help' => t('Bundle for the nlab items.'),
      'filter' => array(
          'handler' => 'views_handler_filter_in_operator',
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );
  
  // Add new field 'title' to the add field list
  //
  $data[NLAB_BASE_TBL]['title'] = array(
      'title' => t('Sculpture Title'),
      'help' => t('Title for the sculpture element.'),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );
  
  // Add new field 'id' to the add field list
  //
  $data[NLAB_BASE_TBL]['nlid'] = array(
      'title' => t('Nlab ID'),
      'help' => t('ID of the nlab element.'),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument',
      ),
  );

  return $data;
}


/**
 * 
 * Implements hook_views_data_alter().
 * 
 */
function nlab_views_data_alter(&$data) {
  
  // Make file extension sortable
  $data['file_managed']['extension']['field']['click sortable'] = 1;
  $data['file_managed']['extension']['sort']['handler'] = 'views_handler_sort';
  $data['file_managed']['extension']['filter']['handler'] = 'views_handler_filter_string';
  $data['file_managed']['extension']['argument']['handler'] = 'views_handler_argument_string';
  
  // Allow to choose bundle from list
  $data[NLAB_ENTITY]['type']['filter']['handler'] = 'nlab_bundle_in_operator';
  
}



/**
 * Custom Views handler : (nlab_bundle_in_operator)
 * Define custom in_opeator
 */
class nlab_bundle_in_operator extends views_handler_filter_in_operator {
  // overwrite the get_value_options function.
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }
    
    // all nlab entity bundles (types)
    $bundles = array();
    $all_bundles = nlab_types();
    foreach ($all_bundles as $key => $abundle) {
      $bundles[$key] = $abundle->name;
    }
    
    // populate filter with bundles
    $this->value_options = $bundles;
    
  }
}

/**
 * 
 * Implements hook_views_default_views()
 * 
 * @return view
 */
function nlab_views_default_views() {
  $views = array();


  $view = new view();
  $view->name = 'nlabs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'nlab';
  $view->human_name = 'nlabs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'nlabs';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '2';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results found.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: nlab: title */
  $handler->display->display_options['fields']['field_painting_title']['id'] = 'field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['table'] = 'field_data_field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['field'] = 'field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['label'] = '';
  $handler->display->display_options['fields']['field_painting_title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_painting_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_painting_title']['element_default_classes'] = FALSE;
  /* Field: nlab: body */
  $handler->display->display_options['fields']['field_painting_body']['id'] = 'field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['table'] = 'field_data_field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['field'] = 'field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['label'] = '';
  $handler->display->display_options['fields']['field_painting_body']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_painting_body']['alter']['more_link'] = TRUE;
  $handler->display->display_options['fields']['field_painting_body']['alter']['more_link_text'] = 'read more';
  $handler->display->display_options['fields']['field_painting_body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_painting_body']['element_label_colon'] = FALSE;
  /* Field: NLab Feed: Bundle */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'nlab';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Sort criterion: nlab: title (field_painting_title) */
  $handler->display->display_options['sorts']['field_painting_title_value']['id'] = 'field_painting_title_value';
  $handler->display->display_options['sorts']['field_painting_title_value']['table'] = 'field_data_field_painting_title';
  $handler->display->display_options['sorts']['field_painting_title_value']['field'] = 'field_painting_title_value';
  $handler->display->display_options['sorts']['field_painting_title_value']['expose']['label'] = 'Title';
  /* Filter criterion: NLab Feed: Bundle */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'nlab';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Bundle';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
  );
  /* Filter criterion: nlab: body (field_painting_body) */
  $handler->display->display_options['filters']['field_painting_body_value']['id'] = 'field_painting_body_value';
  $handler->display->display_options['filters']['field_painting_body_value']['table'] = 'field_data_field_painting_body';
  $handler->display->display_options['filters']['field_painting_body_value']['field'] = 'field_painting_body_value';
  $handler->display->display_options['filters']['field_painting_body_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_painting_body_value']['group'] = 1;
  $handler->display->display_options['filters']['field_painting_body_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_painting_body_value']['expose']['operator_id'] = 'field_painting_body_value_op';
  $handler->display->display_options['filters']['field_painting_body_value']['expose']['label'] = 'Body';
  $handler->display->display_options['filters']['field_painting_body_value']['expose']['operator'] = 'field_painting_body_value_op';
  $handler->display->display_options['filters']['field_painting_body_value']['expose']['identifier'] = 'field_painting_body_value';
  $handler->display->display_options['filters']['field_painting_body_value']['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: NLab Feed: Bundle */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'nlab';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Bundle';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
  );
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'regular_expression';
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
  );
  $handler->display->display_options['filters']['combine']['fields'] = array(
      'field_painting_title' => 'field_painting_title',
      'field_painting_body' => 'field_painting_body',
      'type' => 'type',
  );
  $handler->display->display_options['path'] = 'nlabs';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Nlab List';
  $handler->display->display_options['menu']['description'] = 'Custom entity view integration';
  $handler->display->display_options['menu']['weight'] = '2';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
      'counter' => 'counter',
      'field_painting_title' => 'field_painting_title',
      'field_painting_body' => 'field_painting_body',
      'type' => 'type',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
      'counter' => array(
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
      'field_painting_title' => array(
          'sortable' => 0,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
      'field_painting_body' => array(
          'sortable' => 0,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
      'type' => array(
          'sortable' => 0,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
          'empty_column' => 0,
      ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Bulk operations: NLab Feed */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'nlab';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
      'action::views_bulk_operations_delete_item' => array(
          'selected' => 0,
          'postpone_processing' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
      ),
      'action::nlab_bundle_bulk_delete_items' => array(
          'selected' => 1,
          'postpone_processing' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
      ),
      'action::views_bulk_operations_script_action' => array(
          'selected' => 0,
          'postpone_processing' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
      ),
      'action::views_bulk_operations_modify_action' => array(
          'selected' => 0,
          'postpone_processing' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
          'settings' => array(
              'show_all_tokens' => 1,
              'display_values' => array(
                  '_all_' => '_all_',
              ),
          ),
      ),
      'action::views_bulk_operations_argument_selector_action' => array(
          'selected' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
          'settings' => array(
              'url' => '',
          ),
      ),
      'action::system_send_email_action' => array(
          'selected' => 0,
          'postpone_processing' => 0,
          'skip_confirmation' => 0,
          'override_label' => 0,
          'label' => '',
      ),
  );
  /* Field: NLab Feed: Nlab ID */
  $handler->display->display_options['fields']['nlid']['id'] = 'nlid';
  $handler->display->display_options['fields']['nlid']['table'] = 'nlab';
  $handler->display->display_options['fields']['nlid']['field'] = 'nlid';
  $handler->display->display_options['fields']['nlid']['label'] = '';
  $handler->display->display_options['fields']['nlid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nlid']['element_label_colon'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: nlab: title */
  $handler->display->display_options['fields']['field_painting_title']['id'] = 'field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['table'] = 'field_data_field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['field'] = 'field_painting_title';
  $handler->display->display_options['fields']['field_painting_title']['label'] = 'Title';
  $handler->display->display_options['fields']['field_painting_title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_painting_title']['alter']['path'] = 'nlab/[nlid]';
  $handler->display->display_options['fields']['field_painting_title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_painting_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_painting_title']['element_default_classes'] = FALSE;
  /* Field: nlab: body */
  $handler->display->display_options['fields']['field_painting_body']['id'] = 'field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['table'] = 'field_data_field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['field'] = 'field_painting_body';
  $handler->display->display_options['fields']['field_painting_body']['label'] = 'Description';
  $handler->display->display_options['fields']['field_painting_body']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_painting_body']['alter']['more_link'] = TRUE;
  $handler->display->display_options['fields']['field_painting_body']['alter']['more_link_text'] = 'read more';
  $handler->display->display_options['fields']['field_painting_body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_painting_body']['element_label_colon'] = FALSE;
  /* Field: NLab Feed: Bundle */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'nlab';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: NLab Feed: Bundle */
  $handler->display->display_options['arguments']['type']['id'] = 'type';
  $handler->display->display_options['arguments']['type']['table'] = 'nlab';
  $handler->display->display_options['arguments']['type']['field'] = 'type';
  $handler->display->display_options['arguments']['type']['default_action'] = 'default';
  $handler->display->display_options['arguments']['type']['default_argument_type'] = 'php';
  $handler->display->display_options['arguments']['type']['default_argument_options']['code'] = 'return arg(4);';
  $handler->display->display_options['arguments']['type']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['type']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['type']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  $translatables['nlabs'] = array(
      t('Master'),
      t('nlabs'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('No results found.'),
      t('read more'),
      t('Bundle'),
      t('Title'),
      t('Body'),
      t('Page'),
      t('Search'),
      t('Block'),
      t('NLab Feed'),
      t('Description'),
      t('All'),
  );
  
  
  $views[$view->name] = $view;
  
  return $views;
}