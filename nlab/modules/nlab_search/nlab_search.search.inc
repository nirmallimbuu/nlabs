<?php
/**
 * @file
 * Search feature for nlab search package.
 */

/** * Implementation of hook_apachesolr_update_index(). */
function nlab_search_apachesolr_update_index(&$document, $node) {
//  $document->ss_cck_field_image = $node->field_image[0]['filepath'];
//  $document->ss_cck_field_page_url = $node->field_content_url[0]['display_url'];
}

/** * Implementation of hook_apachesolr_modify_query(). */
function nlab_search_apachesolr_modify_query(&$query, &$params) {
//  $params['fl'] .= ',ss_cck_field_page_url,ss_cck_field_image';
}

/** * Implementation of hook_apachesolr_process_results(). */
function nlab_search_apachesolr_process_results(&$results) {
//  foreach ($results as &$result) {
//    $result['external_url'] = $result['node']->ss_cck_field_page_url;
//    $result['imagefield'] = $result['node']->ss_cck_field_image;
//  }
}

/**
 * Implements hook_apachesolr_entity_info_alter().
 *
 * Adds Text and Photo items to the search index.
 */
function nlab_search_apachesolr_entity_info_alter(&$entity_info) {

  // entities to index
  $types = array('nlab', 'user');
  
  // loop through each entity
  foreach ($types as $type) {
    $entity_info[$type]['indexable'] = TRUE;
    //
    $entity_info[$type]['status callback'] = array('nlab_search_status_callback');
    $entity_info[$type]['document callback'][] = 'nlab_search_solr_document';
    //
    $entity_info[$type]['index_table'] = 'apachesolr_index_entities_nlab';
    $entity_info[$type]['reindex callback'] = 'nlab_search_solr_reindex_entity';
  }

}

/**
 * Callback for the search status. Since all text and photo items are 'published', this is always true.
 */
function nlab_search_status_callback($term, $type) {
  return TRUE;
}

/**
 * Builds the information for a Solr document.
 *
 * @param ApacheSolrDocument $document
 *   The Solr document we are building up.
 * @param stdClass $entity
 *   The entity we are indexing.
 * @param string $entity_type
 *   The type of entity we're dealing with.
 */
function nlab_search_solr_document(ApacheSolrDocument $document, $entity, $entity_type) {
  // Headline
  $documents = array();
  // Solr Mapping
  $siemens_trendlevel_name_solr_mapping = array(
      '0' => 'division',
      '1' => 'searchroom',
      '2' => 'searchfield',
      '3' => 'subfield',
      '4' => 'element',
  );


  // Set up document object
  $document->ss_entity_type = $entity_type;
  
  if (isset($entity->changed)) {
    $document->ds_changed = apachesolr_date_iso($entity->changed);
  }

  if ($entity_type == 'nlab') {

    // Set up document
//    $document = nlab_search_setup_document($document, $entity, $entity_type, $siemens_trendlevel_name_solr_mapping);
    
    $document->label = apachesolr_clean_text($entity->field_painting_title[LANGUAGE_NONE][0]['value']);
    
//    $document->label = apachesolr_clean_text($entity->field_painting_title);
//    $document->content = apachesolr_clean_text($entity->field_painting_body);
    
    
    // Add it to $document array
    $documents[] = $document;

    
  } else if ($entity_type == 'user') {
    if (!$entity->uid)
      return $documents;

    //make sure the user is properly loaded
    $entity = user_load($entity->uid);


    $names = ($entity->name);


    $document->label = $names['label'];

    $document->ss_username = apachesolr_clean_text($entity->name);
    $document->ss_email = apachesolr_clean_text($entity->mail);

    $document->sm_roles = $entity->roles;

    $documents[] = $document;
  }
  
  return $documents;
}


/**
 * 
 * Setup Document : nlab entity
 * 
 */
function nlab_search_setup_document(&$document, $entity, $entity_type, $siemens_trendlevel_name_solr_mapping) {
  
  // Add fields
  $document->label = apachesolr_clean_text($entity->field_painting_title);
  $document->content = apachesolr_clean_text($entity->field_painting_body);
  
}



/**
 * 
 * Reindexing callback for ApacheSolr, for nlab items\
 * 
 */
function nlab_search_solr_reindex_entity() {

    //TODO: Could look this up via entity info
    $entities = array(
        array('table' => 'nlab', 'type' => 'nlab', 'id' => 'nlid'),
        array('table' => 'users', 'type' => 'user', 'id' => 'uid'),
    );

    foreach ($entities as $entity) {
        $indexer_table = apachesolr_get_indexer_table($entity['type']);
        $transaction = db_transaction();

        try {
            db_delete($indexer_table)
                    ->condition('entity_type', $entity['type'])
                    ->execute();

            $select = db_select($entity['table'], 't');
            $select->addField('t', $entity['id'], 'entity_id');
            $select->addExpression("'" . $entity['type'] . "'", 'entity_type');
            $select->addExpression("'" . $entity['type'] . "'", 'bundle');
            $select->addExpression(REQUEST_TIME, 'changed');

            $insert = db_insert($indexer_table)
                    ->fields(array('entity_id', 'entity_type', 'bundle', 'changed'))
                    ->from($select)
                    ->execute();
        } catch (Exception $e) {
            $transaction->rollback();
            watchdog_exception('Apache Solr', $e);
            return FALSE;
        }
    }
    return TRUE;
}





/**
 * 
 * Implements hook_apachesolr_index_documents_alter()
 * 
 */
function nlab_search_apachesolr_index_documents_alter(&$documents, $entity, $entity_type, $env_id) {
  if ($entity_type == 'nlab') {
    foreach ($documents as &$doc) {
      //
    }
  }
}