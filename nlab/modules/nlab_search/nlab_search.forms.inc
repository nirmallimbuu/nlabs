<?php

/**
 * 
 * Form
 * 
 */




/**
 * Search User form
 */
function nlab_search_user_search() {
  
  
  $search_keywords = arg(3);
  
  
  // search box
  $form['search'] = array(
      '#type' => 'textfield',
      '#value' => '',
  );
  
  // search button
  $form['searchbtn'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
      '#ajax' => array(
          'callback' => 'get_users_per_search',
          'wrapper' => 'search-results-users',
          'method' => 'replace',
          'effect' => 'none',
          'event' => 'click',
      ),
  );
  
  // result area
  $form['result'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="search-results-users">',
      '#suffix' => '</div>',
      '#markup' => t('No results found.'),
  );
  
  
  // Assigning weights
  $weight = 0;
  $form['search']['#weight'] = $weight++;
  $form['searchbtn']['#weight'] = $weight++;
  $form['result']['#weight'] = $weight++;
  
  return $form;
}

/**
 * Ajax callback : Upon user search
 */
function get_users_per_search($form, &$form_state) {
  global $base_url;

  
  
  $inputs = $form_state['input'];
  $keyword = $inputs['search'];
  $output = '';
  
  // search fdie("here");or users
  $result = nlab_search_users_get_results($keyword);
  $div_prefix = '<div id="search-results-users">';
  $div_suffix = '</div>';
  
  if (count($result) <= 0) {
    $output = $div_prefix;
    $output .= t('No results found.');
    $output .= $div_suffix;
  } else {
    // display users in result
    $user_row = array();
    foreach ($result as $row) {
      $uid = $row['fields']['entity_id'];
      $user = user_load($uid);
      $options = array(
          'link' => l(t('view profile'), $base_url . '/user/' . $user->uid),
      );
      // create template for each user display
      $user_element = array(
          '#theme' => 'user-result-row',
          '#user' => $user,
          '#options' => $options,
      );
      // add to parent result template
      $user_row[] = drupal_render($user_element);
    }
    $user_result = array(
        '#prefix' => $div_prefix,
        '#suffix' => $div_suffix,
        '#theme' => 'user-result',
        '#users' => $user_row,
    );
    $output .= drupal_render($user_result);
  }
  
  return $output;
}

/**
 * Get users using solr
 */
function nlab_search_users_get_results($keyword) {
  
  $params = array();
  $params['q'] = null;
  // add conditions
  $formatted_filters = array();
  
  $formatted_filters[] = "entity_type:user ";
  if (!empty($keyword)) {
    $formatted_filters[] = "ss_username:" . $keyword . " ";
  }
  
  $params['fq'] = $formatted_filters;
  // add fields to query
  $params['qf'] = array('ss_username', 'ss_email');
  // no. of results
  $params['rows'] = 10;
  
  // run the query
  $solr_results = apachesolr_search_run('', $params, '', '', 0);
  
  return $solr_results;
}