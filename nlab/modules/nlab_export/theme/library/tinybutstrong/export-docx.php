<?php
/**
 *  Settings
 */
// 1:general-info, 2:debug-current-xml, 3:show-xml, 0: none 
$debug = 0;
// 1:download, 0:save as
$file_option = 0;



// Get name
$yourname = 'DefaultGuy';
if (isset($_GET['name'])) {
  $yourname = $_GET['name'];
}



// load the TinyButStrong libraries
include_once('tbs_class.php');
// load the OpenTBS plugin
include_once('tbs_plugin_opentbs.php');
// new instance of TBS
$TBS = new clsTinyButStrong;
// load OpenTBS plugin
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);







$x_num = 3152.456;
$x_pc = 0.2567;
$x_dt = mktime(13, 0, 0, 2, 15, 2010);
$x_bt = true;
$x_bf = false;

$x_delete = 1;



$body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris luctus dolor felis, eget elementum dolor. Donec aliquam dignissim neque. Vestibulum adipiscing mi at enim fermentum ut facilisis ipsum varius. Praesent cursus orci a nibh blandit sit amet volutpat orci condimentum. Praesent quis tellus vehicula eros congue sagittis. Quisque rhoncus interdum lectus at scelerisque. Nullam nunc ante, consectetur sed mattis eget, lobortis ut mi. Pellentesque vel tortor risus, tempor luctus arcu. Praesent non ligula sit amet lectus rutrum sagittis non in nunc. Aliquam non lorem eget velit vestibulum tincidunt sit amet rhoncus mauris. Vestibulum sit amet diam sem, vel tristique erat.';


$footervalue = 'this is footer';

/**
 * Load the template file
 */
//$template = 'testfile.docx';
//$template = 'template.htm';
$template = 'templates/demo_ms_word.docx';
//$TBS->LoadTemplate($template);
//$TBS->LoadTemplate('testfile.docx');
$TBS->LoadTemplate($template);
// Debuging
if ($debug == 1) { 
  $TBS->Plugin(OPENTBS_DEBUG_INFO);
  exit;
} else if ($debug == 2) {
  $TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT);
  exit;
} else if ($debug == 3) {
  $TBS->Plugin(OPENTBS_DEBUG_XML_SHOW);
  exit;
}





// Prepare some data for the demo
$data = array();
$data[] = array('firstname'=>'Nirmal' , 'name'=>'Hill'      , 'number'=>'1523d', 'score'=>200, 'email_1'=>'sh@tbs.com',  'email_2'=>'sandra@tbs.com',  'email_3'=>'s.hill@tbs.com');
$data[] = array('firstname'=>'Anil'  , 'name'=>'Smith'     , 'number'=>'1234f', 'score'=>800, 'email_1'=>'rs@tbs.com',  'email_2'=>'robert@tbs.com',  'email_3'=>'r.smith@tbs.com' );
$data[] = array('firstname'=>'Sijan', 'name'=>'Mac Dowell', 'number'=>'5491y', 'score'=>130, 'email_1'=>'wmc@tbs.com', 'email_2'=>'william@tbs.com', 'email_3'=>'w.m.dowell@tbs.com' );

// Merge data
$TBS->MergeBlock('a,b', $data);


// change chart series
$ChartNameOrNum = 'chart1';
$SeriesNameOrNum = 2;
$NewValues = array( array('Category A','Category B','Category C','Category D'), array(3, 1.1, 4.0, 3.3) );
$NewLegend = "New Nirmal Limbu series 2";
$TBS->PlugIn(OPENTBS_CHART, $ChartNameOrNum, $SeriesNameOrNum, $NewValues, $NewLegend);


// delete comments
$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
    








// Define the name of the output file
$file_name = str_replace('.','_'.date('Y-m-d').'.',$template);
// download or show
if ($file_option == 1) {
  // Download
  $TBS->Show(OPENTBS_DOWNLOAD);
} else {
  // save as file
  $file_name = str_replace('.','_'.'.',$file_name);
  $TBS->Show(OPENTBS_FILE+TBS_EXIT, $file_name);
  
//  $TBS->Show(OPENTBS_STRING);
//  $string = $TBS->Source;
//  echo $string;
//  echo "<br>";
}



?>