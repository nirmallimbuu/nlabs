<?php

/**
 * 
 * Get all the formats selected
 * 
 * @return array
 */
function nlab_export_get_formats($all = true) {
  $formats = array();
  if ($all) {
    $all_formats = variable_get('nlab_export_formats', 'pdf,docx');
  } else {
    $all_formats = variable_get('nlab_export_formats_selected', 'pdf,docx');
  }
  $ex_formats = explode(',', $all_formats);
  $formats = $ex_formats;
  return $formats;
}

/**
 * 
 * Save file formats selected
 * 
 * @param type $formats
 * @return boolean
 */
function nlab_export_set_formats($formats) {
  if (count($formats) > 0) {
    $str_formats = '';
    $total_formats = 0;
    foreach ($formats as $key => $val) {
      print $key . '=' . $val;
      print '<br>';
      // only selected formats pass here
      if ($val != '0') {
        $str_formats .= $key . ',';
        $total_formats++;
      }
    }
    // be sure atleast one is selected
    if (count($total_formats) > 0) {
      //remove last comma (last char)
      $str_formats = substr_replace($str_formats, "", -1);
    }
    //save
    if (!empty($str_formats)) {
      variable_set('nlab_export_formats_selected', $str_formats);
      return true;
    }
  }
  return false;
}

/**
 * 
 * Create Docx file (tinybutstrong)
 * 
 * @global type $user
 * @global type $base_url
 * @param type $filename
 */
function nlab_export_create_file_docx($filename = 'nlab_export_docx.docx', $entity_name, $eid, $libraryname='tinybutstrong') {
  global $user, $base_url;
  $path_to_module = drupal_get_path('module', NLAB_EXPORT_MODULE);
//  $path_to_library = $path_to_module . '/library/tinybutstrong';
//  $path_to_sample_images = $path_to_module.'/templates/sample_images';
  
  // get path to library
  $path_to_library = '';
  // libraries module function
  if (function_exists('libraries_get_path')) {
    $path_to_library = libraries_get_path($libraryname);
  }
  // library not found
  if (empty($path_to_library)) {
    //
    drupal_set_message('Library missing', 'warning');
  }


  // used for altering data (eg. Header, footer)
  $type = array();
  $type[] = 'nlabtype_export_docx';



  // 0. initialize fields
  //
  $body = array();
  $GLOBALS['pagetitle'] = 'Default Titlte';
  $GLOBALS['username'] = $user->name;
  $GLOBALS['headercontent'] = '';
  $GLOBALS['footercontent'] = '';
  // Add images field to doc file
  $images = array();






  /**
   *  1. settings
   */
// 1:general-info, 2:debug-current-xml, 3:show-xml, 0: none 
  $debug = 0;
// 1:download, 2:save as
  $file_option = 1;

// load the TinyButStrong libraries
  include_once($path_to_library . '/tbs_class.php');
// load the OpenTBS plugin
  include_once($path_to_library . '/tbs_plugin_opentbs.php');
// new instance of TBS
  $TBS = new clsTinyButStrong;
// load OpenTBS plugin
  $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

  $x_num = 3152.456;
  $x_pc = 0.2567;
  $x_dt = mktime(13, 0, 0, 2, 15, 2010);
  $x_bt = true;
  $x_bf = false;

  $x_delete = 1;





  /**
   * Load the template file
   */
//  $template = $path_to_library.'/templates/'.$filename;
  $template = $path_to_module . '/templates/nlab_export_docx.docx';



  // check module hooks (module interception
  $get_templates = array();
  // get templates
  $get_templates = module_invoke_all('nlab_export_all_templates');

  if (isset($get_templates) && count($get_templates) > 0) {
    // sort them according to weights
    nlab_drupal_array_sort($get_templates);
    // get first template in line
    $new_module = current($get_templates);
    $template = drupal_get_path('module', $new_module['module']) . '/nlab_export_docx.docx';
  }

//  die("found no templates, have to go with default format");



  $TBS->LoadTemplate($template);







  // load entity
  if (isset($eid)) {
    $load_function = $entity_name . '_load';
    $entity_loaded = $load_function($eid);
    if (isset($entity_loaded)) {
      // Entity title
      $GLOBALS['pagetitle'] = $entity_loaded->title;
      // Body field
      $body[] = $entity_loaded->field_painting_body[LANGUAGE_NONE][0]['value'];
      // Footer content
      $GLOBALS['footercontent'] = 'some footer text';
      // Image fields
      if (isset($entity_loaded->field_painting_image[LANGUAGE_NONE][0])) {
        $style = 'large';
        // loop through all images
        foreach ($entity_loaded->field_painting_image[LANGUAGE_NONE] as $key => $image) {
          $image_full_url = file_create_url($image['uri']);
          $image_full_url_with_style = image_style_url($style, $image['uri']);
//          $filename = $path_to_sample_images.'/pic_5491y.png';
//          $filename = $image_full_url_with_style;
          $image_full_real_path = drupal_realpath($image['uri']);
          $image_filename = $image_full_real_path;
          $images[] = array('filename' => $image_filename);
        }
        //end-loop
      }//end-image-field
      // get fields inserted by other modules
      $get_fields = module_invoke_all('nlab_export_all_fields');
      drupal_sort_weight($get_fields, '#weight');
      if (isset($get_fields) && count($get_fields) > 0) {
        foreach ($get_fields as $key => $afield) {
          // Check if the field we get
          // belongs to the entity loaded
          $afield_name = $afield['field_name'];
          if (isset($entity_loaded->$afield_name)) {
            $afield_field = $entity_loaded->$afield_name;
            // check the field type
            // "text_with_summary"
            if ($afield['type'] == 'text_with_summary') {
              $blocks = array();
              $blocks[] = drupal_html_to_text($afield_field[LANGUAGE_NONE][0]['value']);
              // Add blocks value
              $TBS->MergeBlock('trend_management_summary', $blocks);
            }
            // check the field type
            // "image"
            if ($afield['type'] == 'image') {
              $style = 'medium';
              // loop through all images
              foreach ($afield_field[LANGUAGE_NONE] as $key => $image) {
                $image_full_url = file_create_url($image['uri']);
                $image_full_url_with_style = image_style_url($style, $image['uri']);
//                $filename = $path_to_sample_images.'/pic_5491y.png';
//                $filename = $image_full_url_with_style;
                $image_full_real_path = drupal_realpath($image['uri']);
                $image_filename = $image_full_real_path;
                $images[] = array('filename' => $image_filename);
                // Add images
                $TBS->MergeBlock($afield_name, $images);
              }
              //end-loop
            }
            ///////
          }
        }
      }
      //end-get-fields-hook
    }
  }






// Debuging
  if ($debug == 1) {
    $TBS->Plugin(OPENTBS_DEBUG_INFO);
    exit;
  } else if ($debug == 2) {
    $TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT);
    exit;
  } else if ($debug == 3) {
    $TBS->Plugin(OPENTBS_DEBUG_XML_SHOW);
    exit;
  }


  // Add body field to doc file
  $TBS->MergeBlock('blkbody', $body);

  // Add images
  $TBS->MergeBlock('b', $images);

// delete comments
  $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);




  // 2. Last minute changes through hook 
  //
  // change values such as (pagetitle, footercontent,...)
  drupal_alter($type, $GLOBALS);













  // 3. Overwrite by admin settings
  //
  $val_header = variable_get('nlab_export_val_header', '');
  if (isset($GLOBALS['headercontent'])) {
    if (!empty($val_header)) {
      $GLOBALS['headercontent'] = $val_header;
    }
  }
  $val_footer = variable_get('nlab_export_val_footer', '');
  if (isset($GLOBALS['footercontent'])) {
    if (!empty($val_footer)) {
      $GLOBALS['footercontent'] = $val_footer;
    }
  }





  // 4. Save and Display
// Define the name of the output file
  $file_name = str_replace('.', '_' . date('Y-m-d') . '.', $template);
// download or show
  if ($file_option == 1) {
    // Download
    $TBS->Show(OPENTBS_DOWNLOAD);
  } else if ($file_option == 2) {
    // save as file
    $file_name = str_replace('.', '_' . '.', $file_name);
    $TBS->Show(OPENTBS_FILE + TBS_EXIT, $file_name);
  }
  exit;
}

/**
 * 
 * Create Docx file : (PHPWord)
 * 
 * @global type $user
 * @global type $base_url
 * @param type $filename
 * @param type $entity_name
 * @param type $eid
 */
function nlab_export_create_file_docx_phpword($filename = 'demo_ms_doc.docx', $entity_name, $eid) {
  global $user, $base_url;

  exit;
}

/**
 * 
 * Create PDF file
 * 
 * @param type $entity_name
 * @param type $eid
 */
function nlab_export_create_file_pdf($entity_name, $eid) {
  exit;
}