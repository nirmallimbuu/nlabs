<?php

/**
 * 
 * Admin page
 * 
 * @param type $form
 * @param type $form_state
 * @return type
 */
function nlab_export_settings_page($form, &$form_state) {
  // Check libraries
  $libraries = array();
  $libraries[] = 'tinybutstrong';
  $libraries[] = 'PHPWord';
  $libraries[] = 'PHPExcel-1.7.7';
  $libraries[] = 'tcpdf';
  // Display warning if library not found
  foreach ($libraries as $alibrary) {
    $path_to_library = libraries_get_path($alibrary);
    if (!$path_to_library) {
      drupal_set_message('Library : '.$alibrary.' is missing!', 'warning');
    }
  }
  
  // get all file formats
  $formats = nlab_export_get_formats();
  // get all selected file formats
  $formats_selected = nlab_export_get_formats(false);
  $formats_default = array();
  foreach ($formats_selected as $key=>$val) {
    $formats_default[] = $key;
  }
  // get all contents
  $val_overwrite = variable_get('nlab_export_val_overwrite', '1');
  $val_header = variable_get('nlab_export_val_header', '');
  $val_footer = variable_get('nlab_export_val_footer', '');
  
  // general settings
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#description' => t('Some contents may not be used by some templates.'),
  );
  // supported formats
  $form['settings']['formats'] = array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc($formats),
    '#title' => t('What formats to export?'),
//    '#default_value' => $formats_default,
    '#default_value' => drupal_map_assoc($formats_selected),
  );
  
  $form['settings']['overwrite'] = array(
    '#type' => 'checkbox',
    '#options' => 'checked',
    '#title' => t('Overwrite contents'),
    '#default_value' => $val_overwrite,
  );
  // file contents
  $form['settings']['header'] = array(
    '#type' => 'textfield',
    '#title' => t('Header content :'),
    '#default_value' => $val_header,
    '#states' => array(
      'visible' => array(  // action to take.
        ':input[name="overwrite"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['footer'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer content :'),
    '#default_value' => $val_footer,
    '#states' => array(
      'visible' => array(  // action to take.
        ':input[name="overwrite"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  
  // submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('save'),
  );
  return $form;
}


/**
 * Admin page submit
 * 
 * 
 */
function nlab_export_settings_page_submit($form, $form_state) {
  $values = $form_state['values'];
  
  // Handle formats
  $formats = $values['formats'];
  // save formats
  if (!nlab_export_set_formats($formats)) {
    drupal_set_message('unable to save formats', 'error');
  }
  
  // Save contents only if overwrite is true
  if ($values['overwrite']) {
    // Handle file contents
    $val_header = $values['header'];
    $val_footer = $values['footer'];
    variable_set('nlab_export_val_overwrite', '1');
    if (variable_set('nlab_export_val_header', $val_header)) {
      drupal_set_message('unable to save contents', 'error');
    }
    if (variable_set('nlab_export_val_footer', $val_footer)) {
      drupal_set_message('unable to save contents', 'error');
    }
  } else {
    // save empty
    variable_set('nlab_export_val_overwrite', '0');
    variable_set('nlab_export_val_header', '');
    variable_set('nlab_export_val_footer', '');
  }
  
  
  drupal_set_message('changes saved');
}