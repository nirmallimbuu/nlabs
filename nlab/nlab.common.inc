<?php

/**
 * Generic sorting mechanism.
 *
 * @param $array
 *   The array to sort.
 * @param $key
 *   The index to retrieve. Child keys are passed by comma-seperated values ('item,#weight').
 */
function nlab_drupal_array_sort(&$array, $key = '#weight') {
  static $weightsort = array();
  if (!isset($weightsort[$key])) {
    $weightsort[$key] = create_function('$a,$b', '$keys = explode(",","'.$key.'");
      foreach ($keys as $key) {
        $a = is_array($a) && isset($a[$key]) ? $a[$key] : 0;
        $b = is_array($b) && isset($b[$key]) ? $b[$key] : 0;
      }
      if ($a == $b) {
        return 0;
      }
      return ($a < $b) ? -1 : 1;');
  }
  return uasort($array, $weightsort[$key]);
}





/**
 * Get an array that represents directory tree
 * @param string $directory     Directory path
 * @param bool $recursive         Include sub directories
 * @param bool $listDirs         Include directories on listing
 * @param bool $listFiles         Include files on listing
 * @param regex $exclude         Exclude paths that matches this regex
 */
function nlab_directoryToArray($directory, $recursive = true, $listDirs = false, $listFiles = true, $exclude = '') {
  $arrayItems = array();
  $skipByExclude = false;
  $handle = opendir($directory);
  if ($handle) {
    while (false !== ($file = readdir($handle))) {
      preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file, $skip);
      if ($exclude) {
        preg_match($exclude, $file, $skipByExclude);
      }
      if (!$skip && !$skipByExclude) {
        if (is_dir($directory . DIRECTORY_SEPARATOR . $file)) {
          if ($recursive) {
            $arrayItems = array_merge($arrayItems, directoryToArray($directory . DIRECTORY_SEPARATOR . $file, $recursive, $listDirs, $listFiles, $exclude));
          }
          if ($listDirs) {
            $file = $directory . DIRECTORY_SEPARATOR . $file;
            $arrayItems[] = $file;
          }
        } else {
          if ($listFiles) {
            $file = $directory . DIRECTORY_SEPARATOR . $file;
            $arrayItems[] = $file;
          }
        }
      }
    }
    closedir($handle);
  }
  return $arrayItems;
}







/**
 * 
 *  Get all files and directories in array structure
 * 
 * @param type $dir
 * @return type
 */
function nlab_dirToArray($dir) {
  $result = array();
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value) {
    if (!in_array($value, array(".", ".."))) {
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
        $result[$value] = nlab_dirToArray($dir . DIRECTORY_SEPARATOR . $value);
      } else {
        $result[] = $value;
      }
    }
  }
  return $result;
}



/**
 * 
 *  Get all files and directories in string
 * 
 *  (Notice: extra comma is introduced at the end, so use pop to delete it after explode(',', $val)
 * 
 * 
 * @param type $dir
 * @return type
 */
function nlab_dirToString($dir, $separator) {
  $result = '';
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value) {
    if (!in_array($value, array(".", ".."))) {
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
        $result .= nlab_dirToString($dir . DIRECTORY_SEPARATOR . $value, $separator);
      } else {
        $result .= $value;
        $result .= $separator;
      }
    }
  }
  return $result;
}



/**
 * 
 *  Get all files and directories in string
 * 
 *  (Notice: extra comma is introduced at the end, so use pop to delete it after explode(',', $val)
 * 
 * @param type $dir
 * @param type $separator
 * @return type
 */
function nlab_dirToString_with_parents($dir, $separator, $parent='') {
  $result = '';
  $cdir = scandir($dir);
  $add_this = '';
  foreach ($cdir as $key=>$value) {
    if (!in_array($value, array(".", ".."))) {
      if (!empty($parent)) {
        $add_this = $parent.DIRECTORY_SEPARATOR.$value;
      } else {
        $add_this = $value;
      }
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
        $result .= nlab_dirToString_with_parents($dir . DIRECTORY_SEPARATOR . $value, $separator, $add_this);
      } else {
        $result .= $value;
        $result .= $separator;
      }
    }
  }
  
  return $result;
}






/**
 * 
 * Find file in a directory and its subdiretory as well
 * 
 * @param type $dir
 * @param type $array
 * @return type
 */
function nlab_find_file($dir, $array = array()) {
  $dh = opendir($dir);
  $files = array();
  while (($file = readdir($dh)) !== false) {
    $flag = false;
    if ($file !== '.' && $file !== '..' && !in_array($file, $array)) {
      $files[] = $file;
    }
  }
  return $files;
}

