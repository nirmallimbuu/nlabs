<?php

// Add NLab .................................................................
function nlab_add_page() {
  $the_page = '';
  $item = menu_get_item();
  $links = system_admin_menu_block($item);
  foreach ($links as $link) {
    $items[] = l($link['title'], $link['href'],  
                 $item['localized_options'])
                 . ': ' . filter_xss_admin($link['description']);
  }
  
  // list of nlab items
//  $the_page .= theme('item_list', array('items' => $items));
  
//  $the_page .= drupal_render(block_load('views', 'nlabs-block_1'));
  
  // hooks
  module_invoke_all('nlab_insert');
  
  return $the_page;
}


//..............................................................................

