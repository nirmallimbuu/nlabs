<?php

/**
 * 
 * Rules integration
 * 
 * 
 * There are three parts in rule. (1) Events (2) Condition (3) Action
 * 
 * 
 */

/**
 * 
 * Implementation of hook_rules_event_info().
 * 
 */
function nlab_rules_event_info() {
  return array(
    'nlab_add' => array(
      'label' => t('A new nlab entity is created.'),
      'group' => t('Nlab'),
      'module' => NLAB_MODULENAME,
      'variables' => array(
        'nlab' => array(
          'type' => 'nlab',
          'label' => t('Nlab element'),
        ),
      ),
    ),
    'nlab_update' => array(
      'label' => t('Nlab entity is updated.'),
      'group' => t('Nlab'),
      'module' => NLAB_MODULENAME,
      'variables' => array(
        'nlab' => array(
          'type' => 'nlab',
          'label' => t('Nlab element'),
        ),
      ),
    ),
    'example_rule_event' => array(
      'label' => t('Example rule event'),
      'module' => NLAB_MODULENAME,
      'group' => 'Eureka Rule' ,
      'variables' => array(
        'current_user' => array('type' => 'user', 'label' => t('The current logged in user.')),
        'nlab' => array('type' => 'nlab', 'label' => t('The nlab item.')),
        'some_text' => array('type' => 'text', 'label' => t('Some arbitray text.')),
      ),
    ),  
      
      
  );
}


/**
 * 
 * Implements hook_rules_condition_info()
 * 
 */
function nlab_rules_condition_info() {
  return array(
    // condition: bundle compare
    'nlab_bundle_compare' => array(
      'label' => t('Bundle comparison'), 
      'parameter' => array(
        'nlab' => array(
          'type' => 'nlab',
          'label' => t('Content'),
        ),
        'type' => array(
          'type' => 'list<text>',
          'label' => t('Bundles'),
          'description' => t('Check for selected type of the nlab item.'),
          'options list' => 'rules_nlab_bundles_list',
        ),
      ),
      'operation' => array(
        'type' => 'text',
        'label' => t('Match Bundle'),
        'options list' => 'rules_nlab_condition_operations',
        'restriction' => 'input',
        'optional' => TRUE,
        'default value' => 'AND',
        'description' => t('The nlab must match selected bundle.'),
      ),
      'group' => t('Nlab'),
      'access callback' => TRUE,
      'base' => 'rules_condition_nlab_bundle_compare',
    ),
  );
}
/**
 * Options list callback for nlab bundles.
 */
function rules_nlab_bundles_list($element) {
  return array('painting', 'sculpture');
}

/**
 * Options list callback for the operation parameter of condition nlab with selected bundle.
 */
function rules_nlab_condition_operations() {
  return array(
    'AND' => t('all'),
    'OR' => t('any'),
  );
}


/**
 * Nlab has bundle condition help callback.
 */
function rules_condition_nlab_bundle_compare($element, $type) {
  if (isset($element)) {
    $bundles = rules_nlab_bundles_list($element);
    $selected_types = array();
    foreach ($bundles as $key=>$bundle) {
      if ($element->type == $bundle && in_array($key, $type)) {
        return TRUE;
      }
    }
    return FALSE;
  }
  return FALSE;
}




/**
 * Implements hook_rules_action_info() on behalf of the nlab module.
 * @see rules_core_modules()
 */
function nlab_rules_action_info() {
  $return['nlab_test_action'] = array(
      'label' => t('Test action for Nlab item.'),
      'group' => t('Nlab'),
      'parameter' => array(
          'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'sanitize' => TRUE,
          'translatable' => TRUE,
        ),
      ),
      'provides' => array(
          'nlab' => array(
              'type' => 'nlab',
              'label' => t('Nlab modified')
          ),
      ),
      'base' => 'nlab_rules_action_entity_fetch',
//      'callbacks' => array(
//          'access' => 'rules_action_entity_createfetch_access',
//          'form_alter' => 'rules_action_type_form_alter',
//      ),
  );
  
  return $return;
}



function nlab_test_action() {
  return 'ok';
}
/**
 * 
 * Action Callback
 * 
 * @param text $message
 */
function nlab_rules_action_entity_fetch($message) {
  if (!empty($message)) {
    drupal_set_message($message);
  }
}