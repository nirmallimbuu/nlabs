<?php
/**
 * 
 * Controller class for nlab entity
 * 
 */
class NlabController extends DrupalDefaultEntityController {
  
  public function create($type = '') {
    return (object) array(
      'nlid' => '',
      'type' => $type,
      'title' => '',
    );
  }
  
  public function save($nlab) {
    $transaction = db_transaction();
    try {
      global $user;
      // Determine if we will be inserting a new nlab.
      $nlab->is_new = empty($nlab->nlid);
      // Set the timestamp fields.
      if (empty($nlab->created)) {
        $nlab->created = REQUEST_TIME;
      }
      $nlab->changed = REQUEST_TIME;
      $nlab->revision_timestamp = REQUEST_TIME;
      $update_nlab = TRUE;
      // Give modules the opportunity to prepare field data for saving.
      field_attach_presave('nlab', $nlab);
      if (!$nlab->is_new && !empty($nlab->revision) && 
           $nlab->vid) {
        $nlab->old_vid = $nlab->vid;
        unset($nlab->vid);
      }
      // If this is a new nlab...
      if ($nlab->is_new) {
        // Save the new nlab.
        drupal_write_record('nlab', $nlab);
        // Save the initial revision.
        $this->saveRevision($nlab, $user->uid);
        $op = 'insert';
      }
      else {
        // Save the updated nlab.
        drupal_write_record('nlab', $nlab, 'nlid');
        if (!empty($nlab->revision)) {
          $this->saveRevision($nlab, $user->uid);
        }
        else {
          $this->saveRevision($nlab, $user->uid, TRUE);
          $update_nlab = FALSE;
        }
        $op = 'update';
      }
      // If the revision ID is new or updated, save it to the nlab.
      if ($update_nlab) {
        db_update('nlab')
          ->fields(array('vid' => $nlab->vid))
          ->condition('nlid', $nlab->nlid)
          ->execute();
      }
      // Save fields.
      $function = 'field_attach_' . $op;
      $function('nlab', $nlab);
      module_invoke_all('entity_' . $op, $nlab, 'nlab');
      // Clear internal properties.
      unset($nlab->is_new);
      // Ignore slave server temporarily to give time for the saved 
      // order to be propagated to the slave.
      db_ignore_slave();
      return $nlab;
    } catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('nlab', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  // Save 
  function saveRevision($nlab, $uid, $update = FALSE) {
    // Hold on to the nlab's original creator_uid but swap
    // in the revision's creator_uid for the momentary write.
    $temp_uid = $nlab->uid;
    $nlab->uid = $uid;
    if ($update) {
      drupal_write_record('nlab_revision', $nlab, 'vid');
    } else {
      drupal_write_record('nlab_revision', $nlab);
    }
    // Reset the order's creator_uid to the original value.
    $nlab->uid = $temp_uid;
  }
  
  // Delete
  public function delete($nlids) {
    if (!empty($nlids)) {
      $nlabs = $this->load($nlids, array());
      $transaction = db_transaction();
      try {
        db_delete('nlab')
          ->condition('nlid', $nlids, 'IN')
          ->execute();
        db_delete('nlab_revision')
          ->condition('nlid', $nlids, 'IN')
          ->execute();
        foreach ($nlabs as $nlab_id => $nlab) {
          field_attach_delete('nlab', $nlab);
        }
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('nlab', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }
      module_invoke_all('entity_delete', $nlab, 'nlab');
      // Clear the page and block and nlab caches.
      cache_clear_all();
      $this->resetCache();
    }
    return TRUE;
  }
  
  
  // Get all items of given bundle name
  public function get_all_items($bundle_name='') {
    $query = db_select(NLAB_BASE_TBL, 'nl')
            ->fields('nl');
    if (!empty($bundle_name)) {
      $query->condition('type', $bundle_name);
    }
    return $query;
  }
  
}